﻿using UnityEngine;
using System.Collections;

public class Velocity : MonoBehaviour
{
    private Rigidbody rb;
    public bool goingRight;
    public int velocityX;

    private bool inputSwitch;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.AddForce(getVelocity(), ForceMode.VelocityChange);
    }

    private Vector3 getVelocity()
    {
        if (goingRight)
            return new Vector3(velocityX * -1, 0);
        else
            return new Vector3(velocityX, 0);
    }

    // Update is called once per frame
    void Update()
    {
        inputSwitch = Input.GetButtonDown("Switch");
    }

    public void FixedUpdate()
    {
        if (inputSwitch)
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(getVelocity(), ForceMode.VelocityChange);
            inputSwitch = false;
            goingRight = !goingRight;
        }
    }
}
