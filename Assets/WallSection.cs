﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class WallSection : MonoBehaviour
{

    public int xSize, ySize, zSize;

    private Mesh mesh;
    private Vector3[] vertices;
    private Color32[] cubeUV;

    private void Awake()
    {
        generate();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    /// <summary>
    /// creates a 3 side object with randomized features
    /// </summary>
    private void generate()
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Wall Section";
        createVertices();
        createTriangles();
    }

    private void createVertices()
    {

        int xSide = (ySize + 1) * (zSize + 1) * 2; //Two sides
        int zSide = (zSize - 1) * (ySize + 1); //one side

        vertices = new Vector3[xSide + +zSide];
        cubeUV = new Color32[vertices.Length];

        int v = 0;

        //this method duplicates vertices
        //Makes one circle for each y
        for (int y = 0; y <= ySize; y++)
        {
            for (int z = 0; z <= zSize; z++) //Starts at 0.0 travels along Z to build X side includes corner
            {
                setVertex(v++, 0, y, z);
            }

            for (int x = 1; x < xSize; x++) //Z = zSide, travels along X to build Z side excludes both corners
            {
                setVertex(v++, x, y, zSize);
            }

            //Follows Z to max Z
            for (int z = zSize; z >= 0; z--) //Z & X = z&ySide. travels back along Z to build second X side includes corner
            {
                setVertex(v++, xSize, y, z);
            }
        }
        mesh.vertices = vertices;
        mesh.colors32 = cubeUV;
    }

    private void setVertex(int i, int x, int y, int z)
    {
        vertices[i] = new Vector3(x, y, z);
        cubeUV[i] = new Color32((byte)x, (byte)y, (byte)z, 0);
    }

    private void createTriangles()
    {
        //int quads = (zSize * 2 + xSize + 1) * ySize;
        //int[] triangles = new int[quads * 6];

        int[] trianglesX = new int[(ySize * zSize) * 12];
        int[] trianglesZ = new int[(xSize * ySize) * 6];

        int ring = zSize * 2 + xSize + 1;
        int tZ = 0, tX = 0, v = 0;
        for (int y = 0; y < ySize; y++, v++) //Controls the height of every ring
        {
            for (int q = 0; q < zSize; q++, v++) //Starts at 0.0 travels along Z to build X side
            {
                tX = setQuad(trianglesX, tX, v, v + 1, v + ring, v + ring + 1);
            }
            for (int q = 0; q < xSize; q++, v++) //Z = zSide, travels along X to build Z side
            {
                tZ = setQuad(trianglesZ, tZ, v, v + 1, v + ring, v + ring + 1);
            }
            for (int q = 0; q < zSize; q++, v++) //Z & X = z&ySide. travels back along Z to build second X side
            {
                tX = setQuad(trianglesX, tX, v, v + 1, v + ring, v + ring + 1);
            }
            //setQuad(triangles, t, v, v - ring + 1, v + ring, v + 1); //corrects last triangle of every ring (backside) if I want them
        }
        mesh.subMeshCount = 2;
        mesh.SetTriangles(trianglesZ, 0);
        mesh.SetTriangles(trianglesX, 1);
        mesh.RecalculateNormals();
    }


    /// <summary>
    /// creates quad
    /// </summary>
    /// <param name="triangles">array to keep triangle info. mirrors the vertices array</param>
    /// <param name="i">index</param>
    /// <param name="v00">bottom left</param>
    /// <param name="v10">bottom right</param>
    /// <param name="v01">top left</param>
    /// <param name="v11">top right</param>
    /// <returns></returns>
    private static int setQuad(int[] triangles, int i, int v00, int v10, int v01, int v11)
    {
        triangles[i] = v10;
        triangles[i + 1] = triangles[i + 4] = v11;
        triangles[i + 2] = triangles[i + 3] = v00;
        triangles[i + 5] = v01;
        return i + 6;
    }

    private void OnDrawGizmos()
    {
        if (vertices == null)
        {
            return;
        }
        Gizmos.color = Color.black;
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], 0.05f);
        }
    }
}

